﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using Sapper5.Properties;

namespace Sapper5
{
    public partial class game : Form
    {
        private const int _size = 32;
        private const int field = 36;
        private readonly ArrayList _bombList = new ArrayList();
        private readonly ArrayList _buttonList = new ArrayList();
        private readonly ArrayList _emptyBoxList = new ArrayList();
        private readonly int[] _flagPosition = new int[36];
        private readonly ArrayList _xPositonList = new ArrayList();
        private readonly ArrayList _yPositionList = new ArrayList();
        private int _fieldsInOneRow ;

        private readonly Random rnd = new Random();
        private int _currentBomb;
        private bool _gameStatus = true;
        private int _maxBombs;
        
        private PictureBox _pictureBox = new PictureBox();
        private PictureBox _pictureBox2 = new PictureBox();
        private Point _point;

        private bool test1, test2;
        private int tim;
        private int x = 90, y, bombsAround, num, bmcount;

        public game()
        {
            InitializeComponent();
        }

        private int CoutFieldsInRow()
        {
            _fieldsInOneRow = 0;
            for (int i = field; field % _fieldsInOneRow == 0; _fieldsInOneRow++) {}
            return _fieldsInOneRow;
        }

        private void butt(bool test = false)
        {
            var r = 0;
            if (rnd.Next(0, 5) == 1 && _currentBomb <= _maxBombs) r = 1;

            var pictureBox = new PictureBox();
            if (r == 1 && test)
            {
                pictureBox.BackgroundImage = Resources.bomb;
                _currentBomb++;
            }

            else
            {
                pictureBox.BackgroundImage = Resources._0;
            }

            pictureBox.Size = new Size(32, 32);
            pictureBox.Location = new Point(x, y);
            _xPositonList.Add(x);
            _yPositionList.Add(y);
            _bombList.Add(r);
            _buttonList.Add(pictureBox);
            Controls.Add(pictureBox);
            pictureBox.MouseDown += button1_MouseDown;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CreateBoard();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (num == bmcount)
            {
                MessageBox.Show("U win");
            }
            else
            {
                _gameStatus = false;
                MessageBox.Show("U lose");
            }
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left when _gameStatus:
                {
                    bombsAround = 0;
                    _pictureBox = sender as PictureBox;


                    for (var prestField = 0; prestField < field; prestField++)
                    {
                        test2 = false;
                        _point = new Point((int) _xPositonList[prestField], (int) _yPositionList[prestField]);

                        CheckFieldsAround(prestField, true);
                    }


                    break;
                }
                case MouseButtons.Right when _gameStatus:
                {
                    test1 = false;
                    test2 = false;
                    for (var i = 0; i < field; i++)
                    {
                        _pictureBox2 = sender as PictureBox;
                        var po2 = new Point((int) _xPositonList[i], (int) _yPositionList[i]);
                        if (po2 != _pictureBox2.Location) continue;
                        if (_flagPosition[i] % 2 == 0 && Convert.ToInt32(label2.Text) > 0)
                        {
                            test1 = true;
                            _pictureBox2.BackgroundImage = Resources.flag;
                            if ((int) _bombList[i] == 1) bmcount++;
                            label2.Text = Convert.ToString(Convert.ToInt32(label2.Text) - 1);
                        }

                        if (_flagPosition[i] % 2 == 1)
                        {
                            test2 = true;
                            _pictureBox2.BackgroundImage = Resources._0;
                            if ((int) _bombList[i] == 1) bmcount--;
                            label2.Text = Convert.ToString(Convert.ToInt32(label2.Text) + 1);
                        }

                        if (test1 || test2) _flagPosition[i] += 1;
                    }

                    break;
                }
            }
        }


        private void NewMethod()
        {
            ChangeIfBombsAround();
            SwitchToEmptyBox();
            SwitchToFlagedBox();
        }


        private void CheckFieldsAround(int prestField, bool xd = false)
        {
            if (_pictureBox.Location != _point && xd) return;
            if ((int) _bombList[prestField] == 1 && xd)
            {
                MessageBox.Show("Game Over");
                _gameStatus = false;
                for (var u = 0; u < field; u++)
                    if ((int) _bombList[u] == 1)
                    {
                        var pe = (PictureBox) _buttonList[u];
                        pe.BackgroundImage = Resources.bomb;
                    }
            }
            else
            {
           
                bombsAround = 0;
                if (prestField >= 6 && prestField % 6 == 0 && prestField < 30)
                {
                    CheckRight(prestField);
                    CheckDown(prestField);
                    CheckLeft(prestField);
                    CheckLeftDown(prestField);
                    CheckRightDown(prestField);
                    if (xd) NewMethod();
                }
                else
                {
                    if (prestField == 0)
                    {
                        CheckDown(prestField);
                        CheckRight(prestField);
                        CheckRightDown(prestField);
                        if (xd) NewMethod();
                    }
                    else if (prestField == 30)
                    {
                        CheckDown(prestField);
                        CheckLeft(prestField);
                        CheckLeftDown(prestField);
                        if (xd) NewMethod();
                    }
                    else
                    {
                        if (prestField >= 1 && prestField <= 4)
                        {
                            CheckRight(prestField);
                            CheckUp(prestField);
                            CheckDown(prestField);
                            CheckRightUp(prestField);
                            CheckRightDown(prestField);
                            if (xd) NewMethod();
                        }
                        else
                        {
                            if (prestField == 5)
                            {
                                CheckUp(prestField);
                                CheckRight(prestField);
                                CheckRightUp(prestField);
                                if (xd) NewMethod();
                            }
                            else if (prestField == 35)
                            {
                                CheckLeft(prestField);
                                CheckUp(prestField);
                                CheckLeftUp(prestField);
                                if (xd) NewMethod();
                            }
                            else
                            {
                                if (prestField % 6 == 5 && prestField != 5 && prestField != 35)
                                {
                                    CheckRight(prestField);
                                    CheckUp(prestField);
                                    CheckRightUp(prestField);
                                    CheckLeftUp(prestField);
                                    CheckLeft(prestField);
                                    if (xd) NewMethod();
                                }
                                else if (prestField <= 34 && prestField >= 31)
                                {
                                    CheckLeft(prestField);
                                    CheckLeftUp(prestField);
                                    CheckUp(prestField);
                                    CheckLeftDown(prestField);
                                    CheckDown(prestField);
                                    if (xd) NewMethod();
                                }
                                else
                                {
                                    CheckLeft(prestField);
                                    CheckLeftUp(prestField);
                                    CheckRight(prestField);
                                    CheckRightUp(prestField);
                                    CheckRightDown(prestField);
                                    CheckUp(prestField);
                                    CheckLeftDown(prestField);
                                    CheckDown(prestField);
                                    if (xd) NewMethod();
                                }
                            }
                        }
                    }
                }


                _emptyBoxList.Add(bombsAround);
            }
        }

        private void CheckLeft(int o)
        {
            if ((int) _bombList[o - 6] == 1) bombsAround++;
        }

        private void CheckRight(int o)
        {
            if ((int) _bombList[o + 6] == 1) bombsAround++;
        }

        private void CheckUp(int o)
        {
            if ((int) _bombList[o - 1] == 1) bombsAround++;
        }

        private void CheckDown(int o)
        {
            if ((int) _bombList[o + 1] == 1) bombsAround++;
        }

        private void CheckLeftDown(int o)
        {
            if ((int) _bombList[o - 5] == 1) bombsAround++;
        }

        private void CheckLeftUp(int o)
        {
            if ((int) _bombList[o - 7] == 1) bombsAround++;
        }

        private void CheckRightUp(int o)
        {
            if ((int) _bombList[o + 5] == 1) bombsAround++;
        }

        private void CheckRightDown(int o)
        {
            if ((int) _bombList[o + 7] == 1) bombsAround++;
        }

        private void ChangeIfBombsAround()
        {
            switch (bombsAround)
            {
                case 0:
                    _pictureBox.BackgroundImage = Resources._null;
                    break;
                case 1:
                    _pictureBox.BackgroundImage = Resources._1;
                    break;
                case 2:
                    _pictureBox.BackgroundImage = Resources._2;
                    break;
                case 3:
                    _pictureBox.BackgroundImage = Resources._3;
                    break;
            }
        }

        private void SwitchToEmptyBox()
        {
            for (var prestField = 0; prestField < field; prestField++) CheckFieldsAround(prestField);

            for (var t = 0; t < field; t++)
                if ((int) _bombList[t] != 1 && (int) _emptyBoxList[t] == 0)
                {
                    var pictureBox = (PictureBox) _buttonList[t];
                    pictureBox.Location = new Point((int) _xPositonList[t], (int) _yPositionList[t]);
                    pictureBox.BackgroundImage = Resources._null;
                }
        }

        private void SwitchToFlagedBox()
        {
            for (var i = 0; i < field; i++)
            { 
                if (_flagPosition[i] == 1)
                {
                    var pe = (PictureBox) _buttonList[i];
                    pe.BackgroundImage = Resources.flag;
                }
            }
            
        }

        private void CreateBoard()
        {
          
            _maxBombs = rnd.Next(4, 6);
            for (var i = 0; i < field; i++)
            {
                if (y != 6 * _size)
                {
                    y += _size;
                }
                else
                {
                    y = _size;
                    x += _size;
                }

                butt();
            }

            for (var t = 0; t < field; t++)
                if ((int) _bombList[t] == 1)
                {
                    num++;
                    label2.Text = num.ToString();
                }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tim++;
            label4.Text = tim.ToString();
        }
    }
}